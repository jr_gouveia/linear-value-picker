package br.com.concrete.library.interfaces

interface ValuePickerListener {
    fun OnValueSelected(position: Int, value: String)
}
