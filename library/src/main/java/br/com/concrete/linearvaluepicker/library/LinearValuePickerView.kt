package br.com.concrete.linearvaluepicker.library

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.support.v4.view.animation.FastOutLinearInInterpolator
import android.support.v4.view.animation.FastOutSlowInInterpolator
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.animation.Interpolator
import br.com.concrete.library.interfaces.ValuePickerListener
import br.com.concrete.linearvaluepicker.R

/**
 * Created by joao.gouveia on 01/12/16.
 */
class LinearValuePickerView : View {

    private val radiusPx = convertDpToPixel(6f, context).toInt()
    private val linePx = convertDpToPixel(24f, context).toInt()
    private val strokePx = convertDpToPixel(4f, context).toInt()


    private var color: Int = 0
    private var mBackgroundColor: Int = Color.WHITE
    private var values = arrayOf<String>()
    private var fontRegular: Typeface? = null
    private var fontBold: Typeface? = null
    private var interpolator: Interpolator = FastOutSlowInInterpolator()
    private var textColor = Color.parseColor("#969696")
    private var textSize: Int = 0
    var listener: ValuePickerListener? = null

    private val fillPaint = Paint()
    private val selectedPaint = Paint()
    private val strokePaint = Paint()
    private val linePaint = Paint()
    private val textPaint = Paint()

    private var valueSelectedAnimator: ValueAnimator = ValueAnimator.ofFloat(0f, 1f)
    private var valueHoveredAnimator: ValueAnimator = ValueAnimator.ofFloat(1f, 1f)

    private var selectedRadius: Float = 1f
    private var hoveredRadius: Float = 1f
    private var unhoveredRadius: Float = 1f
    private var unselectedRadius: Float = 1f
    private var textScale: Float = 1f
    private var unselectedTextScale: Float = 1f
    private val selectedScaleRadius = 1.4f

    private var hoveredPosition = -1
    private var unhoveredPosition = -1
    private var unselectedPosition = -2
    var selectedPosition = -2
        set(selectedPosition) {
            if (selectedPosition == this.selectedPosition)
                return

            if (selectedPosition < values.size) {
                this.unselectedPosition = this.selectedPosition
                field = selectedPosition
                animateSelection()

                if (this.selectedPosition >= 0)
                    listener?.OnValueSelected(this.selectedPosition, values[this.selectedPosition])
            }
        }


    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initializeAttrs(attrs)
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initializeAttrs(attrs)
        init()
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        initializeAttrs(attrs)
        init()
    }

    private fun init() {

        val color = Color.TRANSPARENT

        fillPaint.style = Paint.Style.FILL
        fillPaint.isAntiAlias = true
        fillPaint.color = mBackgroundColor

        selectedPaint.style = Paint.Style.FILL_AND_STROKE
        selectedPaint.isAntiAlias = true
        selectedPaint.strokeWidth = strokePx.toFloat()
        selectedPaint.color = color

        strokePaint.style = Paint.Style.STROKE
        strokePaint.isAntiAlias = true
        strokePaint.strokeWidth = strokePx.toFloat()
        strokePaint.color = color


        linePaint.style = Paint.Style.FILL
        linePaint.isAntiAlias = true
        linePaint.color = color
    }


    private fun initializeAttrs(attrs: AttributeSet) {
        val background = background
        if (background is ColorDrawable)
            mBackgroundColor = background.color

        val values = context.obtainStyledAttributes(attrs, R.styleable.ValuePickerView)

        textColor = values.getColor(R.styleable.ValuePickerView_vp_text_color, Color.parseColor("#969696"))
        textSize = values.getDimensionPixelSize(R.styleable.ValuePickerView_vp_text_size, convertDpToPixel(12f, context).toInt())
        color = values.getColor(R.styleable.ValuePickerView_vp_color, Color.GRAY)
        if (values.hasValue(R.styleable.ValuePickerView_vp_fill_color))
            mBackgroundColor = values.getColor(R.styleable.ValuePickerView_vp_fill_color, Color.WHITE)

        val typefaceRegular = values.getString(R.styleable.ValuePickerView_vp_typefaceRegular)
        if (typefaceRegular != null)
            fontRegular = Typeface.createFromAsset(resources.assets, typefaceRegular)

        val typefaceBold = values.getString(R.styleable.ValuePickerView_vp_typefaceBold)
        if (typefaceBold != null)
            fontBold = Typeface.createFromAsset(resources.assets, typefaceBold)

        values.recycle()
    }

    fun setOnValuePickerListener(listener: ValuePickerListener) {
        this.listener = listener
    }

    fun setInterpolator(interpolator: Interpolator) {
        this.interpolator = interpolator
    }

    fun setValues(values: Array<String>) {
        this.values = values
        requestLayout()
    }

    fun setHoveredPosition(hoveredPosition: Int) {
        var hoveredPosition = hoveredPosition
        if (hoveredPosition == this.hoveredPosition)
            return

        if (hoveredPosition == this.selectedPosition)
            hoveredPosition = -1

        if (hoveredPosition < values.size) {
            this.unhoveredPosition = this.hoveredPosition
            this.hoveredPosition = hoveredPosition
            animateHovered()
        }
    }

    private fun animateSelection() {
        if (valueHoveredAnimator.isRunning)
            valueHoveredAnimator.cancel()

        hoveredPosition = -1
        hoveredRadius = 1f


        valueSelectedAnimator.duration = 200
        valueSelectedAnimator.interpolator = interpolator
        valueSelectedAnimator.addUpdateListener { animation ->
            selectedRadius = animation.animatedValue as Float
            textScale = selectedRadius * 0.3f + 1
            selectedRadius *= selectedScaleRadius
            unselectedRadius = selectedScaleRadius - selectedRadius
            unselectedTextScale = 1.3f - textScale + 1
            invalidate()
        }
        valueSelectedAnimator.start()
    }

    private fun animateHovered() {
        valueHoveredAnimator = ValueAnimator.ofFloat(1f, selectedScaleRadius)
        valueHoveredAnimator.duration = 200
        valueHoveredAnimator.interpolator = FastOutLinearInInterpolator()
        valueHoveredAnimator.addUpdateListener { animation ->
            hoveredRadius = animation.animatedValue as Float
            unhoveredRadius = selectedScaleRadius - hoveredRadius + 1
            invalidate()
        }
        valueHoveredAnimator.start()
    }

    @SuppressLint("SwitchIntDef")
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val widthSize = MeasureSpec.getSize(widthMeasureSpec)

        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        val heightSize = MeasureSpec.getSize(heightMeasureSpec)

        val circleDiameterPx = radiusPx * 2
        val desiredHeight = (circleDiameterPx + strokePx) * 4
        var desiredWidth = 0

        if (values.isNotEmpty()) {
            val diameterSum = circleDiameterPx * values.size
            val strokeSum = strokePx * 2 * values.size
            val lineSum = linePx * (values.size - 1)
            desiredWidth = diameterSum + strokeSum + lineSum
        }

        var width: Int
        var height: Int

        when (widthMode) {
            MeasureSpec.EXACTLY -> width = widthSize
            MeasureSpec.AT_MOST -> width = Math.min(desiredWidth, widthSize)
            else -> width = desiredWidth
        }

        when (heightMode) {
            MeasureSpec.EXACTLY -> height = heightSize
            MeasureSpec.AT_MOST -> height = Math.min(desiredHeight, heightSize)
            else -> height = desiredHeight
        }

        if (width < 0)
            width = 0

        if (height < 0)
            height = 0

        setMeasuredDimension(width, height)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        val middle = height / 2
        val y = middle + middle / 2

        if (values.isNotEmpty())
            drawLine(canvas, y)


        for (i in values.indices) {
            val x = getXCoordinate(i)
            drawCircle(canvas, i, x, y)
            drawText(canvas, values[i], i, x, middle)
        }
    }

    private fun drawText(canvas: Canvas, value: String, position: Int, x: Int, y: Int) {
        var newTextSize = textSize.toFloat()
        var ny = y.toFloat()
        if (position == this.selectedPosition) {
            newTextSize *= textScale
            ny = y - (10 * (textScale - 1))
            getFontType(textScale)
        } else if (position == unselectedPosition) {
            newTextSize *= unselectedTextScale
            ny = y - (10 * (unselectedTextScale - 1))
            getFontType(unselectedTextScale)
        } else {
            getFontType(1f)
        }

        textPaint.textSize = newTextSize
        textPaint.style = Paint.Style.FILL
        textPaint.textAlign = Paint.Align.LEFT
        textPaint.color = textColor

        canvas.drawText(value, x - textPaint.measureText(value) / 2, ny, textPaint)
    }

    private fun getFontType(scale: Float) {
        if (scale > 1) {
            textPaint.typeface = if (fontBold != null) fontBold else Typeface.DEFAULT_BOLD
        } else
            textPaint.typeface = if (fontRegular != null) fontRegular else Typeface.DEFAULT
    }

    private fun drawLine(canvas: Canvas, y: Int) {
        val paint: Paint = linePaint

        paint.color = color
        paint.strokeWidth = (strokePx / 2).toFloat()
        canvas.drawLine(getXCoordinate(0).toFloat(), y.toFloat(), getXCoordinate(values.size - 1).toFloat(), y.toFloat(), paint)
    }


    //TODO: refatorar e organizar código
    private fun drawCircle(canvas: Canvas, position: Int, x: Int, y: Int) {
        val radius = radiusPx.toFloat()

        var shouldDrawCircle = true

        if (position == this.selectedPosition && selectedRadius > 1) {
            shouldDrawCircle = false
        }
        if (position == unselectedPosition && unselectedRadius > 1) {
            shouldDrawCircle = false
        }
        if (position == unhoveredPosition && unhoveredRadius > 1) {
            shouldDrawCircle = false
        }

        if (shouldDrawCircle) {
            val paint: Paint = strokePaint
            paint.strokeWidth = strokePx.toFloat()

            paint.color = color

            val fill = fillPaint
            canvas.drawCircle(x.toFloat(), y.toFloat(), radius, paint)
            canvas.drawCircle(x.toFloat(), y.toFloat(), radius, fill)
        }

        if (position == this.selectedPosition && selectedRadius > 0) {
            var radiusSelected = radiusPx.toFloat()
            radiusSelected *= selectedRadius
            selectedPaint.color = color
            canvas.drawCircle(x.toFloat(), y.toFloat(), radiusSelected, selectedPaint)
        }

        if (position == unselectedPosition && unselectedRadius > 0) {
            val radiusUnselected = radiusPx * unselectedRadius
            selectedPaint.color = color
            canvas.drawCircle(x.toFloat(), y.toFloat(), radiusUnselected, selectedPaint)
        }

        if (position == hoveredPosition && hoveredRadius > 0) {
            val radiusHovered = radiusPx * hoveredRadius
            val paint: Paint = strokePaint
            paint.strokeWidth = strokePx.toFloat()

            paint.color = color

            val fill = fillPaint
            canvas.drawCircle(x.toFloat(), y.toFloat(), radiusHovered, paint)
            canvas.drawCircle(x.toFloat(), y.toFloat(), radiusHovered, fill)
        }

        if (position == unhoveredPosition && unhoveredRadius > 0) {
            val radiusUnhovered = radiusPx * unhoveredRadius
            val paint: Paint = strokePaint
            paint.strokeWidth = strokePx.toFloat()

            paint.color = color

            val fill = fillPaint
            canvas.drawCircle(x.toFloat(), y.toFloat(), radiusUnhovered, paint)
            canvas.drawCircle(x.toFloat(), y.toFloat(), radiusUnhovered, fill)
        }


    }

    private fun getXCoordinate(position: Int): Int {
        val actualViewWidth = calculateActualViewWidth()
        var x = (width - actualViewWidth) / 2

        if (x < 0) {
            x = 0
        }

        for (i in values.indices) {
            x += radiusPx + strokePx
            if (position == i) {
                return x
            }

            x += radiusPx + linePx
        }

        return x
    }

    private fun getPositionTouched(posX: Int, posY: Int): Int {
        if (posX < 0 || posY < 0 || posY > height)
            return -1

        val actualViewWidth = calculateActualViewWidth()
        var x = (width - actualViewWidth) / 2

        if (x < 0) {
            x = 0
        }

        var initialX: Int
        var finalX: Int
        val variation = radiusPx + strokePx + linePx / 2

        for (i in values.indices) {
            x += radiusPx + strokePx

            initialX = x - variation
            finalX = x + variation

            if (posX in initialX..finalX)
                return i

            x += radiusPx + linePx
        }

        return -1
    }

    private fun calculateActualViewWidth(): Int {
        var width = 0
        val diameter = radiusPx * 2 + strokePx

        for (i in values.indices) {
            width += diameter

            if (i < values.size - 1) {
                width += linePx
            }
        }

        return width
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {

        val eventAction = event.action
        val x = event.x.toInt()
        val y = event.y.toInt()
        val position = getPositionTouched(x, y)

        when (eventAction) {
            MotionEvent.ACTION_DOWN -> if (position >= 0)
                setHoveredPosition(position)
            MotionEvent.ACTION_UP -> if (position >= 0)
                selectedPosition = position
            else
                setHoveredPosition(position)
            MotionEvent.ACTION_MOVE -> setHoveredPosition(position)
        }

        return true
    }


    companion object {
        fun convertDpToPixel(dp: Float, context: Context): Float {
            val resources = context.resources
            val metrics = resources.displayMetrics
            val px = dp * (metrics.densityDpi / 160f)
            return px
        }
    }

}


