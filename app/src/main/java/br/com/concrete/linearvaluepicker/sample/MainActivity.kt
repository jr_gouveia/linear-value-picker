package br.com.concrete.linearvaluepicker.sample

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.TextView
import br.com.concrete.library.interfaces.ValuePickerListener
import br.com.concrete.linearvaluepicker.library.LinearValuePickerView

class MainActivity : AppCompatActivity(), ValuePickerListener {
    internal var values = arrayOf("10", "20", "30", "40", "50", "60", "70"  )

    val text by lazy { findViewById<TextView>(R.id.selectedValue) }
    val view by lazy { findViewById<LinearValuePickerView>(R.id.linearvaluepicker) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        view.setValues(values)
        view.setOnValuePickerListener(this)
    }

    override fun OnValueSelected(position: Int, value: String) {
        Log.d("SampleActivity", "posicao $position  -  value $value")
        text.text = value
    }
}
